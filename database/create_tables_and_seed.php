<?php
require_once('./connect_database.php');
require_once('../utils/random_string.php');

// sql to create categories table
$sql = "CREATE TABLE $table_categories (
    category_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    category_name VARCHAR(30) NOT NULL,
    post_created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    post_edited_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )";
    
if (mysqli_query($conn, $sql)) {
    echo "Table $table_categories created successfully";
} else {
    echo "Error creating table: " . mysqli_error($conn);
}

echo "<br>";

// create some categories
$sql = "INSERT INTO $table_categories (category_name)
VALUES ('Games');";
$sql .= "INSERT INTO $table_categories (category_name)
VALUES ('Sports');";
$sql .= "INSERT INTO $table_categories (category_name)
VALUES ('Movies');";
$sql .= "INSERT INTO $table_categories (category_name)
VALUES ('Books');";
$sql .= "INSERT INTO $table_categories (category_name)
VALUES ('Cooking')";

if (mysqli_multi_query($conn, $sql)) {
  echo "New records in $table_categories table created successfully";
} else {
  echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

echo "<br>";

while(mysqli_more_results($conn))
   mysqli_next_result($conn);

// sql to create posts table
$sql = "CREATE TABLE $table_posts (
    post_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    post_name VARCHAR(100) NOT NULL,
    post_description VARCHAR(5000) NOT NULL,
    post_image VARCHAR(300) NOT NULL,
    post_created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    post_edited_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    category_id int(6) UNSIGNED,
    FOREIGN KEY (category_id) REFERENCES $table_categories(category_id) 
    )";
    
if (mysqli_query($conn, $sql)) {
    echo "Table $table_posts created successfully";
} else {
    echo "Error creating table: " . mysqli_error($conn);
}

echo "<br>";

// delete all images from the posts folder
array_map('unlink', array_filter((array) glob("../img/posts/*")));

// generate images from the inserted items below
$image1 = '../img/migrate/1-minecraft.jpg';
$output1 = '../img/posts/1-' . generateRandomString(15) . '.jpg';
file_put_contents($output1, file_get_contents($image1));

$image2 = '../img/migrate/2-tetris.jpg';
$output2 = '../img/posts/2-' . generateRandomString(15) . '.jpg';
file_put_contents($output2, file_get_contents($image2));

$image3 = '../img/migrate/3-fio-maravilha.jpg';
$output3 = '../img/posts/3-' . generateRandomString(15) . '.jpg';
file_put_contents($output3, file_get_contents($image3));

$image4 = '../img/migrate/4-vincent.jpeg';
$output4 = '../img/posts/4-' . generateRandomString(15) . '.jpeg';
file_put_contents($output4, file_get_contents($image4));

$image5 = '../img/migrate/5-lotr.jpg';
$output5 = '../img/posts/5-' . generateRandomString(15) . '.jpg';
file_put_contents($output5, file_get_contents($image5));

$image6 = '../img/migrate/6-swordfish.jpg';
$output6 = '../img/posts/6-' . generateRandomString(15) . '.jpg';
file_put_contents($output6, file_get_contents($image6));

// queries to posts
$sql = "INSERT INTO $table_posts (post_name, post_description, post_image, category_id)
VALUES ('The spetacular world of Minecraft', 'Minecraft is a sandbox video game developed by the Swedish video game developer Mojang Studios. ... In Minecraft, players explore a blocky, procedurally generated 3D world with virtually infinite terrain, and may discover and extract raw materials, craft tools and items, and build structures or earthworks.', '$output1', 1);";
$sql .= "INSERT INTO $table_posts (post_name, post_description, post_image, category_id)
VALUES ('How tetris changed everything', 'Tetris, video game created by Russian designer Alexey Pajitnov in 1985 that allows players to rotate falling blocks strategically to clear levels. ... The goal of the game is to prevent the blocks from stacking up to the top of the screen for as long as possible.', '$output2', 1);";
$sql .= "INSERT INTO $table_posts (post_name, post_description, post_image, category_id)
VALUES ('Fio Maravilha isnt the same', 'João Batista de Sales, better known as Fio Maravilha, is a former Brazilian football player. In Brazil he played for Flamengo, Paysandu Sport Club, CEUB, Desportiva and São Cristóvão. Later he moved to the United States, where he played for the New York Eagles, the Montebello Panthers, and the San Francisco Mercury.', '$output3', 2);";
$sql .= "INSERT INTO $table_posts (post_name, post_description, post_image, category_id)
VALUES ('Loving Vincent', 'A man visits the last hometown of Vincent Van Gogh, to deliver his final letter. Taking place in 1890 France, the man investigates what might have taken place in the last days of the painter.', '$output4', 3);";
$sql .= "INSERT INTO $table_posts (post_name, post_description, post_image, category_id)
VALUES ('Lord of the rings is wrong and i can prove it', 'The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien. The films are subtitled The Fellowship of the Ring, The Two Towers, and The Return of the King.', '$output5', 3);";
$sql .= "INSERT INTO $table_posts (post_name, post_description, post_image, category_id)
VALUES ('Swordfish filet', 'Swordfish, also known as broadbills in some countries, are large, highly migratory, predatory fish characterized by a long, flat, pointed bill. They are a popular sport fish of the billfish category, though elusive. Swordfish are elongated, round-bodied, and lose all teeth and scales by adulthood.', '$output6', 5)";

if (mysqli_multi_query($conn, $sql)) {
  echo "New records in $table_posts table created successfully";
} else {
  echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

echo "<br>";

while(mysqli_more_results($conn))
   mysqli_next_result($conn);

// sql to create users table
$sql = "CREATE TABLE $table_users (
    user_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(60) NOT NULL,
    user_email VARCHAR(40) NOT NULL,
    user_password VARCHAR(180) NOT NULL,
    user_is_admin BOOLEAN DEFAULT FALSE,
    post_created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    post_edited_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )";
    
if (mysqli_query($conn, $sql)) {
    echo "Table $table_users created successfully";
} else {
    echo "Error creating table: " . mysqli_error($conn);
}

echo "<br>";

// create admin and common user
$default_password = '123456';
$hashed_default_password = md5($default_password);
$sql = "INSERT INTO $table_users (user_name, user_email, user_password, user_is_admin)
VALUES ('Admin user', 'admin@gmail.com', '$hashed_default_password', TRUE);";
$sql .= "INSERT INTO $table_users (user_name, user_email, user_password)
VALUES ('Common user', 'user@gmail.com', '$hashed_default_password')";

if (mysqli_multi_query($conn, $sql)) {
  echo "New records in $table_users table created successfully";
} else {
  echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

disconnect_db($conn);
?>