<?php
require_once(dirname(__FILE__) . '/../database/db_functions.php');
require_once(dirname(__FILE__) . '/random_string.php');

function check_field($text) {
  $text = trim($text);
  $text = stripslashes($text);
  $text = htmlspecialchars($text);
  return $text;
}

$error = false;
$conn = connect_database();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // validate login
  if (strpos($_SERVER['SCRIPT_NAME'], 'login')) {
    if (empty($_POST["email"])) {
      $email_error = "Email is mandatory.";
      $error = true;
    }
    else {
      $email = check_field($_POST["email"]);

      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $invalid_email_error = "Invalid email.";
        $error = true;
      }
    }

    if (empty($_POST["password"])) {
      $password_error = "Password is mandatory.";
      $error = true;
    }
    else {
      $password = check_field($_POST["password"]);
    }

    if (!empty($_POST["remember"])) {
      $remember = $_POST["remember"] == 'on';
    }
  } else if (strpos($_SERVER['SCRIPT_NAME'], 'register')) {  // validate register
    if (empty($_POST["email"])) {
      $email_error = "Email is mandatory.";
      $error = true;
    }
    else {
      $email = check_field($_POST["email"]);
      $sql = "SELECT user_id FROM $table_users WHERE user_email = '$email';";
      $result = mysqli_query($conn, $sql);
      $user = mysqli_fetch_assoc($result);
      if (isset($user["user_id"])) {
        $email_error = "Email is already picked.";
        $error = true;
      } else {        
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          $invalid_email_error = "Invalid email.";
          $error = true;
        }
      }
    }

    if (empty($_POST["username"])) {
      $username_error = "Username is mandatory.";
      $error = true;
    }
    else {
      $username = check_field($_POST["username"]);
    }

    if (empty($_POST["password"])) {
      $password_error = "Password is mandatory.";
      $error = true;
    }
    else {
      $password = check_field($_POST["password"]);

      if (empty($_POST["password-confirmation"])) {
        $password_confirmation_error = "Password confirmation is mandatory.";
        $error = true;
      }
      else {
        $password_confirmation = check_field($_POST["password-confirmation"]);

        if ($password !== $password_confirmation) {
          $password_mismatch_error = "Passwords doesn't match!";
          $error = true;
        }
      }
    }

    if (!empty($_POST["remember"])) {
      $remember = $_POST["remember"] == 'on';
    }
  } else if (strpos($_SERVER['SCRIPT_NAME'], 'post')) {  // validate post edit/create
    $created_at = $_POST["post_created_at"];
    if (empty($_POST["title"])) {
      $title_error = "Title is mandatory.";
      $error = true;
    }
    else {
      $name = check_field($_POST["title"]);
    }

    if (empty($_POST["description"])) {
      $description_error = "Description is mandatory.";
      $error = true;
    }
    else {
      $description = check_field($_POST["description"]);
    }

    if (empty($_POST["category"])) {
      $category_error = "Category is mandatory.";
      $error = true;
    }
    else {
      $category = check_field($_POST["category"]);
      $sql = "SELECT category_id FROM $table_categories WHERE category_name = '$category';";
      $result = mysqli_query($conn, $sql);
      $category = mysqli_fetch_assoc($result);
      if (!isset($category["category_id"])) {
        $category_error = "Category doesn't exist.";
        $error = true;
      } else {        
        $category = $category["category_id"];
      }
    }

    if ((empty($_FILES["file"]) || $_FILES["file"]["size"] === 0) && (!isset($_POST["old_file"]) || strlen($_POST["old_file"]) === 0)) {
      $file_error = "Image is mandatory.";
      $error = true;
    }
    else {
      if ((empty($_FILES["file"]) || $_FILES["file"]["size"] === 0) && isset($_POST["old_file"]) && strlen($_POST["old_file"]) > 0) {
        $file = $_POST["old_file"];
      } else {
        if (isset($_POST["old_file"]) && strlen($_POST["old_file"]) > 0)
          unlink('../' . $_POST["old_file"]);
        $target_dir = '../img/posts/';     // to save in the database
        $target_dir2 = '../../img/posts/';     // reference from post.php file
        $string = generateRandomString(15);
        $file = $target_dir . $string . basename($_FILES["file"]["name"]);
        $file2 = $target_dir2 . $string . basename($_FILES["file"]["name"]);
        $imageFileType = strtolower(pathinfo($file2, PATHINFO_EXTENSION));
        $check = getimagesize($_FILES["file"]["tmp_name"]);
        if($check !== false) {
          if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png") {
            $file_error = "Image should be jpg, jpeg or png.";
            $error = true;
          } else {
            if ($_FILES["file"]["size"] > 1024*1024) {
              $file_error = "Image is too big.";
              $error = true;
            } else {
              if (!move_uploaded_file($_FILES["file"]["tmp_name"], $file2)) {
                $file_error = "Não foi possível salvar a imagem.";
                $error = true;
              }
            }
          }
        } else {
          $file_error = "Invalid image.";
          $error = true;
        }
      }
    }
  }
}

disconnect_db($conn);
?>